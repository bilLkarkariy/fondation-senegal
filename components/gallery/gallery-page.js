import React from "react";
import GalleryCard from "./gallery-card";
import galleryImage1 from "../../assets/images/gallery/gallery-1-1.jpeg";
import galleryImage2 from "../../assets/images/gallery/gallery-1-2.jpeg";
import galleryImage3 from "../../assets/images/gallery/gallery-1-3.jpeg";
import galleryImage4 from "../../assets/images/gallery/gallery-1-4.jpeg";
import galleryImage5 from "../../assets/images/gallery/gallery-1-5.jpeg";
import galleryImage6 from "../../assets/images/gallery/gallery-1-6.jpeg";
import galleryImage7 from "../../assets/images/gallery/gallery-1-7.jpeg";
import galleryImage8 from "../../assets/images/gallery/gallery-1-8.jpeg";
import galleryImage9 from "../../assets/images/gallery/gallery-1-9.jpeg";
import galleryImage10 from "../../assets/images/gallery/gallery-1-10.jpeg";
import galleryImage11 from "../../assets/images/gallery/gallery-1-11.jpeg";

const GalleryPage = () => {
  return (
    <section className="gallery-page pt-120 pb-120">
      <div className="container">
        <div className="gallery-3-col">
          <GalleryCard image={galleryImage1} />
          <GalleryCard image={galleryImage2} />
          <GalleryCard image={galleryImage3} />
          <GalleryCard image={galleryImage4} />
          <GalleryCard image={galleryImage5} />
          <GalleryCard image={galleryImage6} />
          <GalleryCard image={galleryImage7} />
          <GalleryCard image={galleryImage8} />
          <GalleryCard image={galleryImage9} />
          <GalleryCard image={galleryImage10} />
          <GalleryCard image={galleryImage11} />

        </div>
      </div>
    </section>
  );
};

export default GalleryPage;
