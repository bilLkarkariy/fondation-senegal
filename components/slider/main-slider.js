import React from "react";
import { Container, Row, Col } from "react-bootstrap";
import { Swiper, SwiperSlide } from "swiper/react";
import SwiperCore, { Autoplay, Pagination, EffectFade } from "swiper";

import banner1 from "../../assets/images/main-slider/slider-1-1.jpg";
import banner2 from "../../assets/images/main-slider/slider-1-2.jpg";
import banner3 from "../../assets/images/main-slider/slider-1-3.jpg";

SwiperCore.use([Autoplay, Pagination, EffectFade]);

const MainSlider = () => {
  const mainSlideOptions = {
    slidesPerView: 1,
    loop: true,
    effect: "fade",
    pagination: {
      el: "#main-slider-pagination",
      type: "bullets",
      clickable: true
    },
    autoplay: {
      delay: 5000
    }
  };
  return (
    <section className="main-slider">
      <Swiper {...mainSlideOptions}>
        <SwiperSlide>
          <div
            className="image-layer"
            style={{ backgroundImage: `url(${banner1})` }}
          ></div>

          <Container>
            <Row className="row justify-content-end">
              <Col xl={7} lg={12} className="text-right">
                <p>Nous disons  <strong>STOP</strong>  à l’immigration clandestine !</p>
                <h2>Jeunesse <br /> {"&"} <br />bien-être.</h2>
                <a
                  href="/cause-details#message"
                  data-target=".donate-options"
                  className="scroll-to-target thm-btn"
                >
                  Faire un Don
                </a>
              </Col>
            </Row>
          </Container>
        </SwiperSlide>
        <SwiperSlide>
          <div
            className="image-layer"
            style={{ backgroundImage: `url(${banner3})` }}
          ></div>

          <Container>
            <Row className="row justify-content-end">
              <Col xl={7} lg={12} className="text-right">
                <p>Nous voulons un meilleur bien-être de la jeunesse en Afrique.</p>
                <h2>
                <small>Projet <br /> d'éducation <br /> agroécologique.</small>
                </h2>
                <a
                  href="#"
                  data-target=".donate-options"
                  className="scroll-to-target thm-btn"
                >
                Donation
                </a>
              </Col>
            </Row>
          </Container>
        </SwiperSlide>
        <SwiperSlide>
          <div
            className="image-layer"
            style={{ backgroundImage: `url(${banner2})` }}
          ></div>

          <Container>
            <Row className="row justify-content-end">
              <Col xl={8} lg={12} className="text-right">
                <p>Soutenir une meilleure vie des jeunes, et encourager un meilleur avenir !</p>
                <h2>
                  Un Don <br /> pour changer <br /> une vie.
                </h2>
                <a
                  href="/cause-details#message"
                  data-target=".donate-options"
                  className="scroll-to-target thm-btn "
                >
                  Faire un Don
                </a>
              </Col>
            </Row>
          </Container>
        </SwiperSlide>
        <div className="swiper-pagination" id="main-slider-pagination"></div>
      </Swiper>
    </section>
  );
};

export default MainSlider;
