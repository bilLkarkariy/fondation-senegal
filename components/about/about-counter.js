import React, { useState } from "react";
import CountUp from "react-countup";
import VisibilitySensor from "react-visibility-sensor";
import { Container, Row, Col } from "react-bootstrap";
import heartImage from "../../assets/images/shapes/heart-2-1.png";
import aboutImage from "../../assets/images/resources/about-counter-1-1.jpg";
import aboutHeart from "../../assets/images/shapes/about-count-heart-1-1.png";
import { motion } from "framer-motion";

const variants = {
  open: { opacity: 1, x: 0 },
  closedRight: { opacity: 0, x: "-100px" },
  closedLeft: { opacity: 0, x: "100px" },
};

const AboutCounter = () => {
  const [counter, setCounter] = useState({
    startCounter: false,
  });

  const onVisibilityChange = (isVisible) => {
    if (isVisible) {
      setCounter({ startCounter: true });
    }
  };
  return (
    <section className="about-counter pt-120">
      <Container>
        <Row>
          <Col lg={6}>
            <motion.div
              animate={counter.startCounter ? "open" : "closedRight"}
              variants={variants}
              transition={{ duration: 0.15, ease: "easeInOut" }}
            >
              <div className="block-title">
                <p>
                  <img src={heartImage} width="15" alt="" />
                  Aidons les jeunes enfants maintenant
                </p>
                <h3>Créer un lien avec la terre.</h3>
              </div>
              <p className="about-counter__text">
                Les terres utilisées par les élève seront gérés de façon <br />
                à ce que chaque élève soit responsable de une ou plusieurs
                cultures sur sa parcelle distribuées selon les capacités de
                l’élève : <br />
              </p>
              <ul className="list-unstyled ul-list-one">
                <li>Un pot pour les petits.</li>
                <li>Une planche pour les plus grands.</li>
                <li>
                  Plusieurs planches, ou un jardin pour ceux qui ont fait leurs
                  preuves.
                </li>
                <li>
                  Soutien pour l’installation d’un jardin en agroforesterie.
                </li>
              </ul>
              <div className="about-counter__count">
                <h3 className="odometer">
                  <VisibilitySensor
                    onChange={onVisibilityChange}
                    offset={{ top: 10 }}
                    delayedCall
                  >
                    <CountUp end={counter.startCounter ? 7500 : 0} />
                  </VisibilitySensor>
                  €
                </h3>
                <p>
                  Les campagnes de dons <br /> sont en cours
                </p>
              </div>
            </motion.div>
          </Col>
          <Col lg={6}>
            <motion.div
              animate={counter.startCounter ? "open" : "closedLeft"}
              variants={variants}
              transition={{ duration: 0.3 }}
            >
              <div className="about-counter__image clearfix">
                <div className="about-counter__image-content">
                  <img src={aboutHeart} alt="" />
                  <p>Nous sommes là pour les soutenir à chaque étape.</p>
                </div>
                <img src={aboutImage} alt="" className="float-left" />
              </div>
            </motion.div>
          </Col>
        </Row>
      </Container>
    </section>
  );
};

export default AboutCounter;
