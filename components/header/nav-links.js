import React from "react";
import Link from "next/link";

const NavLinks = ({ extraClassName }) => {
  const handleDropdownStatus = (e) => {
    let clickedItem = e.currentTarget.parentNode;
    clickedItem.querySelector(".dropdown-list").classList.toggle("show");
  };
  return (
    <ul className={`main-menu__list ${extraClassName}`}>
      <li className="dropdown">
        <Link href="/">
            <a>Accueil</a>
        </Link>
      </li>
      <li className="dropdown">
        <Link href="/cause-details">
            <a>En détails</a>
        </Link>
      </li>
      {/* <li className="dropdown">
      <Link href="/about">
              <a>A Propos</a>
            </Link>
      </li> */}
      <li className="dropdown">
      <Link href="/gallery">
              <a>Galerie</a>
            </Link>
      </li>
   <li>
        <Link href="/contact">
          <a>Contact</a>
        </Link>
      </li>
      {/* <li className="search-btn search-toggler">
        <span>
          <i className="azino-icon-magnifying-glass"></i>
        </span>
      </li> */}
    </ul>
  );
};

export default NavLinks;
