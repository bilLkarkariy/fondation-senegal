import React from "react";
import { Container, Row, Col } from "react-bootstrap";
import BlockTitle from "../block-title";

const ContactFormOne = () => {
  const Mailto = ({ email, subject = "", body = "", children }) => {
    let params = subject || body ? "?" : "";
    if (subject) params += `subject=${encodeURIComponent(subject)}`;
    if (body) params += `${subject ? "&" : ""}body=${encodeURIComponent(body)}`;

    return <a href={`mailto:${email}${params}`}>{children}</a>;
  };

  return (
    <section className="contact-page pt-120 pb-80">
      <Container>
        <Row>
          <Col lg={5}>
            <div className="contact-page__content mb-40">
              <Mailto
                email="jetbe.fd@hotmail.com"
                subject="Contact Fondation Jeunesse & Bien-être"
                body=""
              >
                {" "}
                <BlockTitle
                  title={`N'hésitez pas à nous écrire \nun message.`}
                  tagLine="Contactez-nous"
                />{" "}
              </Mailto>

              <div className="footer-social black-hover">
                <a href="#" aria-label="twitter">
                  <i className="fab fa-twitter"></i>
                </a>
                <a href="#" aria-label="facebook">
                  <i className="fab fa-facebook-square"></i>
                </a>
                <a href="#" aria-label="pinterest">
                  <i className="fab fa-pinterest-p"></i>
                </a>
                <a href="#" aria-label="instagram">
                  <i className="fab fa-instagram"></i>
                </a>
              </div>
            </div>
          </Col>
          <Col lg={7}>
            {/* <form className="contact-form-validated contact-page__form form-one mb-40">
              <div className="form-group">
                <div className="form-control">
                  <label htmlFor="name" className="sr-only">
                    Nom
                  </label>
                  <input
                    type="text"
                    name="name"
                    id="name"
                    placeholder="Nom"
                  />
                </div>
                <div className="form-control">
                  <label htmlFor="email" className="sr-only">
                    email
                  </label>
                  <input
                    type="text"
                    name="email"
                    id="email"
                    placeholder="Addresse mail"
                  />
                </div>
                <div className="form-control">
                  <label htmlFor="subject" className="sr-only">
                    Objet
                  </label>
                  <input
                    type="text"
                    name="subject"
                    id="subject"
                    placeholder="Objet"
                  />
                </div>
                <div className="form-control form-control-full">
                  <label htmlFor="message" className="sr-only">
                    message
                  </label>
                  <textarea
                    name="message"
                    placeholder="Votre Message"
                    id="message"
                  ></textarea>
                </div>
                <div className="form-control form-control-full">
                  <button type="submit" className="thm-btn ">
                    Envoyer
                  </button>
                </div>
              </div>
            </form>
            <div className="result"></div> */}
          </Col>
        </Row>
      </Container>
    </section>
  );
};

export default ContactFormOne;
