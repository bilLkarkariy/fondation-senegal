import React, { useState } from "react";
import ModalVideo from "react-modal-video";
import { Container, Row, Col } from "react-bootstrap";
import videoBg from "../../assets/images/shapes/video-bg-1-1.png";
import videoImage from "../../assets/images/resources/video-1-1.png";
import { motion } from 'framer-motion';
import VisibilitySensor from "react-visibility-sensor";


const variants = {
  open: { opacity: 1, x: 0 }, 
  closed: { opacity: 0, x: "-300px" }, 
};


const VideoCardTwo = () => {
  const [isOpen, setOpen] = useState(false);
  const [isVisible, setIsVisible] = useState(false);
  const onVisibilityChange = (isVisible) => {
    if (isVisible) {
      setIsVisible(true);
    }
  };

  return (
    <VisibilitySensor
      onChange={onVisibilityChange}
      offset={{ top: 10 }}
      delayedCall
    >
      <section className="video-card-two">
        <ModalVideo
          channel="youtube"
          autoplay
          isOpen={isOpen}
          videoId="L61p2uyiMSo"
          onClose={() => setOpen(false)}
        />
        <motion.div animate={isVisible ? "open" : "closed"}
          variants={variants}  
          transition={{ duration: 0.5 }}>
          <Container>
            <div
              className="inner-container"
              style={{ backgroundImage: `url(${videoBg})` }}
            >
              <Row className="align-items-center">
                <Col lg={3}>
                  <div className="video-card-two__box">
                    {/* <img src={videoImage} alt="" /> */}
                    {/* <span
                      className="video-card-two__box-btn video-popup"
                      onClick={() => setOpen(true)}
                    >
                      <i className="fa fa-play"></i>
                    </span> */}
                  </div>
                </Col>
                <Col lg={4}>
                  <h3>L’enseignement de l’agriculture est une matière vivante</h3>
                </Col>
                <Col lg={5} >
                  <p className="p-text-highlight">
                    permettant aux jeunes de
                    renouer un lien avec la terre, d’obtenir une source de revenu, et de participer au
                    développement du pays.
              </p>
                </Col>
              </Row>
            </div>
          </Container>
        </motion.div>

      </section>
    </VisibilitySensor>
  );
};

export default VideoCardTwo;
