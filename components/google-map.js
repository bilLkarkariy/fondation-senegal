import React from "react";

const GoogleMap = ({ extraClass }) => {
  return (
    <div className={`google-map__${extraClass}`}>
      <iframe
        title="template google map"
        src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d30856.34663917663!2d-17.171346627104423!3d14.82282608830845!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0xec1a39ed123b311%3A0xf3b1fb6fe5ea3396!2zR29yb20sIFPDqW7DqWdhbA!5e0!3m2!1sfr!2sfr!4v1607959926370!5m2!1sfr!2sfr"
        className={`map__${extraClass}`}
        allowFullScreen
      ></iframe>
    </div>
  );
};

export default GoogleMap;