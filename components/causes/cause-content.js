import React, { useState, useEffect } from "react";
import { Container, Row, Col, Alert, Spinner } from "react-bootstrap";
import causeImage1 from "../../assets/images/causes/cause-d-1-1.jpg";
import organizer1 from "../../assets/images/causes/organizer-1-1.jpg";
import { CardElement, useStripe, useElements } from "@stripe/react-stripe-js";
import Router from "next/router";

export async function fetchGetJSON(url) {
  try {
    const data = await fetch(url).then((res) => res.json());
    return data;
  } catch (err) {
    throw new Error(err.message);
  }
}

export async function fetchPostJSON(url, data) {
  try {
    // Default options are marked with *
    const response = await fetch(url, {
      method: "POST", // *GET, POST, PUT, DELETE, etc.
      mode: "cors", // no-cors, *cors, same-origin
      cache: "no-cache", // *default, no-cache, reload, force-cache, only-if-cached
      credentials: "same-origin", // include, *same-origin, omit
      headers: {
        "Content-Type": "application/json",
        // 'Content-Type': 'application/x-www-form-urlencoded',
      },
      redirect: "follow", // manual, *follow, error
      referrerPolicy: "no-referrer", // no-referrer, *client
      body: JSON.stringify(data || {}), // body data type must match "Content-Type" header
    });
    return await response.json(); // parses JSON response into native JavaScript objects
  } catch (err) {
    throw new Error(err.message);
  }
}

const CauseContent = () => {
  const [values, setValues] = useState({ name: "", email: "", amount: 0 });

  const [payment, setPayment] = useState({ status: "initial", id: "" });
  const [errorMessage, setErrorMessage] = useState("");
  const [formatAmount, setFormatAmount] = useState(0);

  const stripe = useStripe();
  const elements = useElements();
  const validEmailRegex = RegExp(
    /^(([^<>()\[\]\.,;:\s@\"]+(\.[^<>()\[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i
  );

  const PaymentStatus = ({ status }) => {
    switch (status) {
      case "processing":
      case "requires_payment_method":
      case "requires_confirmation":
        return (
          <Spinner animation="border" role="status">
            <span className="sr-only">Chargement...</span>
          </Spinner>
        );
      case "requires_action":
        return (
          <Spinner animation="border" role="status">
            <span className="sr-only">Chargement...</span>
          </Spinner>
        );

      case "succeeded":
        postData();
        setTimeout(() => Router.push("/"), 5000);
        return (
          <Alert variant="success">
            <Alert.Heading>Merci pour votre don !</Alert.Heading>
          </Alert>
        );
      case "error":
        return (
          <Alert variant="danger" onClose={() => setShow(false)} dismissible>
            <Alert.Heading>
              Une erreur est survenue, veuillez recommencer et/ou nous contacter
            </Alert.Heading>
            <p className="error-message">{errorMessage}</p>
          </Alert>
        );

      default:
        return null;
    }
  };

  const postData = async () => {
    // const response = await fetchPostJSON(process.env.ENDPOINT + '/api/donations/', {
    //   amount: values.amount,
    //   payment_id: payment.id,
    //   email: values.email,
    //   name: values.name
    // });
  };

  const handleInputChange = (e) => {
    const { name, value } = e.target;
    if (name == "amount") {
      if (Number.isInteger(Number(value))) {
        if (value != 0) {
          setFormatAmount(e.currentTarget.value);
          setValues({ ...values, [name]: value * 100 });
        }
      } else {
      }
    } else if (name == "anonyme") {
      setValues({ ...values, [name]: e.target.checked });
    } else {
      setValues({ ...values, [name]: value });
    }
  };

  const handleSubmit = async (e) => {
    // Block native form submission.
    e.preventDefault();
    setPayment({ status: "processing" });
    if (!stripe || !elements) {
      // Stripe.js has not loaded yet. Make sure to disable
      // form submission until Stripe.js has loaded.
      return;
    }
    if (!validEmailRegex.test(values.email)) {
      setErrorMessage("Email invalide");
      return;
    }

    const response = await fetchPostJSON("/api/payment_intents", {
      amount: values.amount,
      payment_id: payment.id,
      email: values.email,
      // name: values.name
    });
    setPayment(response);

    if (response.statusCode === 500) {
      setPayment({ status: "error" });
      setErrorMessage(response.message);
      return;
    }

    // Get a reference to a mounted CardElement. Elements knows how
    // to find your CardElement because there can only ever be one of
    // each type of element.
    const cardElement = elements.getElement(CardElement);
    const { error, paymentIntent } = await stripe.confirmCardPayment(
      response.client_secret,
      {
        payment_method: {
          card: cardElement,
        },
      }
    );

    if (error) {
      setPayment({ status: "error" });
      setErrorMessage(error.message ?? "An unknown error occured");
    } else if (paymentIntent) {
      setPayment(paymentIntent);
    }
  };

  const CARD_OPTIONS = {
    iconStyle: "solid",
    style: {
      base: {
        iconColor: "#6772e5",
        color: "#6772e5",
        fontWeight: "500",
        fontFamily: "Roboto, Open Sans, Segoe UI, sans-serif",
        fontSize: "16px",
        fontSmoothing: "antialiased",
        ":-webkit-autofill": {
          color: "#fce883",
        },
        "::placeholder": {
          color: "#6772e5",
        },
      },
      invalid: {
        iconColor: "#ef2961",
        color: "#ef2961",
      },
    },
  };
  return (
    <section className="cause-details blog-details  pt-120 pb-40">
      <Container>
        <Row>
          <Col md={12} lg={12}>
            <div className="cause-details__content">
              <div className="cause-card">
                <div className="cause-card__inner">
                  <div className="cause-card__image">
                    <img src={causeImage1} alt="" />
                  </div>
                  <div className="cause-card__content"></div>
                </div>
              </div>
              <h3>Actions</h3>
              <h4>Éducation</h4>
              <p>
                L’agroécologie répond à des besoins humains en termes de
                sécurité alimentaire, de source de revenu, et de bien-être
                général. C’est un ensemble de fondements qui s’adaptent du
                jardin familial, jusqu’au producteur céréalier, de quelques m2 à
                plusieurs centaines d’hectares, mécanisé ou non. Les cours
                dispensés aux enfants et adolescents ont pour objectif de former
                des agriculteurs qui comprennent ce qu’ils font, avec un bagage
                permettant d'appréhender les différentes réalités du métier et,
                de faire les meilleurs choix durant leur future carrière.
              </p>
              <p>
                Ces cours sont centrés sur la vie du sol, simplement car nous
                sommes vivants, nous mangeons des organismes qui ont été
                vivants, et nous interagissons avec la vie autour de nous, il
                est normal que le sol soit vivant également. Connaitre les lois
                du vivant et s’y conformer afin de faire les meilleurs choix
                pour la vie privée et professionnelle, pour se respecter
                soi-même et respecter la vie dans ses différentes
                manifestations, partager l’abondance pour que Allah soit
                satisfait de nous, pour toutes ces raisons nous travaillons à
                l’acquisition et la diffusion de savoirs utiles, théoriques et
                pratiques, vérifiables par l’expérience.
              </p>
              <p>
                Comme nous le savons, une alimentation et une activité saine
                produit un corps sain. À cela nous ajoutons le préliminaire : un
                sol vivant, avec une activité biologique forte et diversifiée
                produira naturellement des plantes saines. Aujourd’hui nous
                connaissons des pratiques agricoles qui augmentent la fertilité
                naturelle, biologique du sol, plus encore que s’il était
                abandonné à la nature, c’est-à-diree la biomasse globale
                produite est supérieur à celle d’une parcelle équivalente qui
                part naturellement vers la forêt.
              </p>
              <p>
                L'intention de ces cours est de fournir à ces enfants une
                formation professionnelle complète, un savoir utile pour
                eux-mêmes, pour leur bien-être, présent et futur, ainsi que pour
                le bien-être de la société.
              </p>
              <div className="cause-card__bottom">
                <a href="#message" className="thm-btn dynamic-radius">
                  Faire un don
                </a>
                <a href="#message" className="cause-card__share">
                  <i className="azino-icon-share"></i>
                </a>
              </div>
              <h4>Organisation des cours</h4>

              <img src={organizer1} alt="" width="100%" />
              <div className="cause-details__presentations">
                <h4>Programme agronomique</h4>
                <ol>
                  <li>Le sol</li>
                  <li>La cellule</li>
                  <li>Nutritions de la plante</li>
                  <li>Stockages de l’eau et des nutriments</li>
                  <li>L’influence du climat sur les cultures</li>
                  <li>Différentes agricultures</li>
                  <li>Économies/finance</li>
                </ol>
              </div>
            </div>
            <h4>Contenu pédagogique en agroécologie</h4>
            <div className="cause-details__presentations display-block ">
              <h5>Niveau débutant :</h5>
              <ul>
                <li>À quoi servent les plantes ?</li>
                <li>
                  Découverte du bouturage en mettant simplement des bouts de
                  plante dans une bouteille d’eau.
                </li>
                <li>Distribution de graines.</li>
                <li>Espace de culture : pots, planches de cultures...</li>
              </ul>
              <h5>Niveau avancé :</h5>
              <ul>
                <li>Technique du Saî</li>
                <li>Compost / matière organique</li>
                <li>Paillage</li>
              </ul>
              <h5>Perfectionnement :</h5>
              <ul>
                <li>Gestion de l’arrosage, problématique de l’érosion.</li>
                <li>Organisation des cultures.</li>
                <li>Production de semences</li>
                <li>Espace de culture : pots, planches de cultures...</li>
              </ul>
            </div>
            <h3 id="message" className="blog-details__title">
              Votre Don
            </h3>
            <form
              onSubmit={handleSubmit}
              className="contact-form-validated contact-page__form form-one mb-80"
            >
              <div className="form-group">
                <div className="form-control">
                  <input
                    type="text"
                    name="name"
                    onChange={handleInputChange}
                    placeholder="Nom"
                  />
                </div>
                <div className="form-control">
                  <input
                    type="text"
                    name="email"
                    onChange={handleInputChange}
                    placeholder="Addresse mail"
                  />
                </div>
                <div className="form-control">
                  <input
                    type="text"
                    name="amount"
                    value={formatAmount}
                    onChange={handleInputChange}
                    placeholder="Montant du don"
                  />
                </div>
                <Col md="12">
                  <CardElement options={CARD_OPTIONS} />
                </Col>
                <div className="form-control form-control-full">
                  <button
                    type="submit"
                    className="thm-btn dynamic-radius"
                    disabled={
                      !["initial", "error"].includes(payment.status) || !stripe
                    }
                  >
                    Envoyer
                  </button>
                  <PaymentStatus status={payment.status} />
                </div>
              </div>
            </form>
            <div className="result"></div>
          </Col>
        </Row>
      </Container>
    </section>
  );
};

export default CauseContent;
