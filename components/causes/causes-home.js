import React from "react";
import Link from "next/link";
import { Swiper, SwiperSlide } from "swiper/react";
import { Container, Row, Col } from "react-bootstrap";
import heartImage from "../../assets/images/shapes/heart-2-1.png";
import causeImage1 from "../../assets/images/causes/cause-1-1.jpg";
import causeImage2 from "../../assets/images/causes/cause-1-2.jpg";
import causeImage3 from "../../assets/images/causes/cause-1-3.jpg";
import causeImage4 from "../../assets/images/causes/cause-1-4.jpg";
import causeImage5 from "../../assets/images/causes/cause-1-5.jpg";

const CausesHomeData = [
  {
    image: causeImage1,
    progressCount: 20,
    raised: "25,270",
    goal: "30,000",
    title: "I. Les Terres",
    text: "Dans un premier temps nous occuperons 3000 m2 avec les enfants au Lac Rose.",
    link: "/cause-details"
  },
  {
    image: causeImage2,
    progressCount: 40,
    raised: "25,270",
    goal: "30,000",
    title: "II. Accueil des Enfants",
    text: "Nous commencerons par l’accueil de 33 enfants de 3 communes différentes afin de lancer le projet, lancer l’organisation en place, puis nous augmenterons la capacité d’accueil jusqu’a l’effectif optimal pour le bien-être de tous.",
    link: "/cause-details"
  },
  {
    image: causeImage3,
    progressCount: 60,
    raised: "25,270",
    goal: "30,000",
    title: "III. Les Locaux",
    text: "3 salles de classe sont construites, il ne reste que les finitions (tables, tableau…). Sur les parcelles pédagogiques, nous installerons des tentes, puis nous construiront des habitats selon les fondements agroécologiques.",
    link: "/cause-details"
  },
  {
    image: causeImage4,
    progressCount: 80,
    raised: "25,270",
    goal: "30,000",
    title: "IV. Intégration",
    text: "Selon les attributions de terres, nous installerons des jardins pédagogiques sur une part, et des cultures rémunératrices sur l’autre part.",
    link: "/cause-details"
  },
  {
    image: causeImage5,
    progressCount: 100,
    raised: "25,270",
    goal: "30,000",
    title: "V. Propagation",
    text: "Une fois que le modèle est installé et fonctionnel, avec les différents partenaires, nous multiplierons les centres d’accueils là où cela est possible.",
    link: "/cause-details"
  }
];

const CausesHome = () => {
  const swiperParams = {
    slidesPerView: 3,
    spaceBetween: 30,
    breakpoints: {
      0: {
        slidesPerView: 1,
        spaceBetween: 30
      },
      375: {
        slidesPerView: 1,
        spaceBetween: 30
      },
      575: {
        slidesPerView: 1,
        spaceBetween: 30
      },
      768: {
        slidesPerView: 1,
        spaceBetween: 30
      },
      991: {
        slidesPerView: 2,
        spaceBetween: 30
      },
      1199: {
        slidesPerView: 2,
        spaceBetween: 30
      },
      1200: {
        slidesPerView: 3,
        spaceBetween: 30
      }
    }
  };
  return (
    <section className="causes-page causes-home pt-120 pb-120">
      <Container>
        <Row className=" align-items-start align-items-md-center flex-column flex-md-row mb-60">
          <Col lg={7}>
            <div className="block-title">
              <p>
                <img src={heartImage} width="15" alt="" />
                Grandes étapes du projet
              </p>
              <h3>
                Un objectif ambitieux <br /> une équipe motivée.
              </h3>
            </div>
          </Col>
          <Col lg={5} className=" d-flex">
            <div className="my-auto">
              <p className="block-text pr-10 mb-0">
                Comme vous le savez, les moyens mis à disposition sont inutiles s’il n’y a pas de personnel,
                et des gens motivés ne peuvent pas s’investir correctement s’il n’y a pas de moyen.
              </p>
            </div>
          </Col>
        </Row>
        <Swiper {...swiperParams}>
          {CausesHomeData.map(
            (
              { image, progressCount, raised, goal, title, text, link },
              index
            ) => (
                <SwiperSlide key={`cause-card-key-${index}`}>
                  <div className="cause-card">
                    <div className="cause-card__inner">
                      <div className="cause-card__image">
                        <img src={image} alt="" />
                      </div>
                      <div className="cause-card__content">
                        <div className="cause-card__top">
                          <div className="cause-card__progress">
                            <span
                              style={{ width: `${progressCount}%` }}
                              className="wow cardProgress"
                              data-wow-duration="1500ms"
                            >
                              <b>
                                <i>{progressCount}</i>%
                            </b>
                            </span>
                          </div>
                          {/* <div className="cause-card__goals">
                            <p>
                              <strong>Raised:</strong> ${raised}
                            </p>
                            <p>
                              <strong>Goal:</strong> ${goal}
                            </p>
                          </div> */}
                        </div>
                        <h3>
                          <Link href={link}>
                            <a>{title}</a>
                          </Link>
                        </h3>
                        <p>{text}</p>
                        <div className="cause-card__bottom">
                          <Link href={link}>
                            <a className="thm-btn ">Faire un don</a>
                          </Link>
                          <Link href="/cause-details#message">
                            <a
                              className="cause-card__share"
                              aria-label="share postr"
                            >
                              <i className="azino-icon-share"></i>
                            </a>
                          </Link>
                        </div>
                      </div>
                    </div>
                  </div>
                </SwiperSlide>
              )
          )}
        </Swiper>
      </Container>
    </section>
  );
};

export default CausesHome;
