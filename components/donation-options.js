import React from "react";
import { Container, Row, Col } from "react-bootstrap";
import heartImage from "../assets/images/shapes/heart-2-1.png";
import Router from "next/router";

const DonationOptions = () => {
  const handleSubmit = async (e) => {
    Router.push("/cause-details");
  };
  return (
    <section className="donate-options pt-120">
      <Container>
        <Row>
          <Col lg={6}>
            <div className="donate-options__content">
              <div className="block-title">
                <p>
                  <img src={heartImage} width="15" alt="" />
                  Faire un don
                </p>
                <h3>
                  Donner un coup de main
                  <br /> <small> pour des enfants dans le besoin.</small>
                </h3>
              </div>
              <p className="text-justify">
                Une subvention de 7500€ est nécessaire au lancement du projet.{" "}
                <br />
                Nous sollicitons également l’usage de terres agricoles. Nous
                avons d’autres besoin comme des habits pour les enfants, des
                fournitures scolaires, des outils, des tentes...
              </p>
              <div className="donate-options__call">
                <i className="azino-icon-telephone"></i>
                <div className="donate-options__call-content">
                  <p>
                    Vous avez des questions concernant les dons ? <br />{" "}
                    <span>Contactez-nous maintenant :</span>{" "}
                    <a href="">jetbe.fd@hotmail.com</a>
                  </p>
                </div>
              </div>
            </div>
          </Col>
          <Col lg={6}>
            <form
              onSubmit={handleSubmit}
              className="donate-options__form wow fadeInUp"
              data-wow-duration="1500ms"
            >
              <h3 className="text-center">Faire un don maintenant</h3>
              <p className="text-center">
                Comme vous le savez, les moyens mis à disposition sont inutiles
                s’il n’y a pas de personnel, et des gens motivés ne peuvent pas
                s’investir correctement s’il n’y a pas de moyen.
              </p>
              <br />
              <button type="submit" className="thm-btn ">
                Envoyer votre don
              </button>
            </form>
          </Col>
        </Row>
      </Container>
    </section>
  );
};

export default DonationOptions;
