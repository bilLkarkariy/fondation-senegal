import React from "react";
import Layout from "../components/layout";
import HeaderOne from "../components/header/header-one";
import StickyHeader from "../components/header/sticky-header";
import PageHeader from "../components/page-header";
import CauseContent from "../components/causes/cause-content";
import Footer from "../components/footer";

const CauseDetails = () => {
  return (
    <Layout pageTitle="Détails Projet || Fondation Jeunesse & Bien-Être">
      <HeaderOne />
      <StickyHeader />
      <PageHeader title="Le Projet" crumbTitle="Projet" />
      <CauseContent />
      <Footer />
    </Layout>
  );
};

export default CauseDetails;
