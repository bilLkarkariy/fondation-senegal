
import Stripe from 'stripe'
const stripe = new Stripe(process.env.STRIPE_SECRET_KEY, {
    // https://github.com/stripe/stripe-node#configuration
    apiVersion: '2019-12-03',
})

export default async (req, res) => {
    if (req.method === 'POST') {
        // Create a new customer object
        const customer = await stripe.customers.create({
            email: req.body.email,
        });

        // Recommendation: save the customer.id in your database.
        res.send({ customer });
    } else {
        res.setHeader('Allow', 'POST')
        res.status(405).end('Method Not Allowed')
    }
}
