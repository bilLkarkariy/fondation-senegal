
import Stripe from 'stripe'
const stripe = new Stripe(process.env.STRIPE_SECRET_KEY, {
    // https://github.com/stripe/stripe-node#configuration
    apiVersion: '2019-12-03',
})

export default async (req, res) => {
   // Attach the payment method to the customer
   try {
    await stripe.paymentMethods.attach(req.body.customerId.paymentMethodId, {
      customer: req.body.customerId.customerId,
    });
  } catch (error) {
    return res.status('402').send({ error: { message: error.message } });
  }
  // Change the default invoice settings on the customer to the new payment method
  await stripe.customers.update(
    req.body.customerId.customerId,
    {
      invoice_settings: {
        default_payment_method: req.body.customerId.paymentMethodId,
      },
    }
  );    // Create the subscription
  const subscription = await stripe.subscriptions.create({
    customer: req.body.customerId.customerId,
    items: [{ price: "plan_HIROZT4WuqfX89" }],
    expand: ['latest_invoice.payment_intent'],
  });
  res.send(subscription);
}
