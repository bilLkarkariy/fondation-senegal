
import Stripe from 'stripe'
const stripe = new Stripe(process.env.STRIPE_SECRET_KEY, {
  // https://github.com/stripe/stripe-node#configuration
  apiVersion: '2019-12-03',
})

export default async (req, res) => {

  if (req.method === 'POST') {
    console.log("email: ", req.body);
    const { amount, email } = req.body;
    try {
      const customer = await stripe.customers.create({
        email: email,
      });
      // await res.send({ customer });

  
      // Recommendation: save the customer.id in your database.
      // res.send({ customer });
      // Create PaymentIntent from body params.
      const params = {
        payment_method_types: ['card'],
        amount: amount,
        currency: 'eur',
        customer: customer.id
      }
      const payment_intent = await stripe.paymentIntents.create(
        params,
      )

      res.status(200).json(payment_intent)

      // try {
      //   await stripe.paymentMethods.attach(payment_intent.paymentMethodId, {
      //     customer: customer.customerId,
      //   });
      // } catch (error) {
      //   return res.status('402').send({ error: { message: error.message } });
      // }

    } catch (err) {

      res.status(500).json({ statusCode: 500, message: err.message })
    }
  } else {
    res.setHeader('Allow', 'POST')
    res.status(405).end('Method Not Allowed')
  }
}
